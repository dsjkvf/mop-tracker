module bitbucket.com/dsjkvf/mop-tracker

go 1.13

require (
	github.com/Knetic/govaluate v3.0.0+incompatible // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mop-tracker/mop v0.2.0
	github.com/nsf/termbox-go v0.0.0-20200418040025-38ba6e5628f1
)

replace github.com/mop-tracker/mop v0.2.0 => ./mop
