mop-tracker
===========


## About

This is a fork of [mop market/stocks tracker](https://github.com/mop-tracker/mop). All the credits naturally go to the original author, [Michael Dvorkin](https://github.com/michaeldv), and I strongly encourage you to use [the original version](https://github.com/mop-tracker/mop).

This fork, however, contains several patches (which may be found in the [`patches`](https://bitbucket.org/dsjkvf/mop-tracker/src/master/patches/) folder and can be applied separately if needed), such as:

  - prices for WTI oil and Gold are no longer displayed in the top toolbar (instead, use tickers, like `CL=F` or `GC=F`, to track those);
  - special tickers (like commodities or currency rates) now have some text decorations to visually distinguish them from other tickers.

## Download

Binaries are available from the [Downloads](https://bitbucket.org/dsjkvf/mop-tracker/downloads/) page.

## License
[MIT](https://bitbucket.org/dsjkvf/mop-tracker/src/master/LICENSE)
