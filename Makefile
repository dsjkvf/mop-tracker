# Copyright (c) 2013-2019 by Michael Dvorkin and contributors. All Rights Reserved.
# Use of this source code is governed by a MIT-style license that can
# be found in the LICENSE file.

VERSION = 0.2.1-mod

buildall:
	GOOS=darwin  GOARCH=amd64 go build $(GOFLAGS) -o ./bin/mop-$(VERSION)-osx-64
	GOOS=freebsd GOARCH=amd64 go build $(GOFLAGS) -o ./bin/mop-$(VERSION)-freebsd-64
	GOOS=linux   GOARCH=amd64 go build $(GOFLAGS) -o ./bin/mop-$(VERSION)-linux-64
	GOOS=linux   GOARCH=386   go build $(GOFLAGS) -o ./bin/mop-$(VERSION)-linux-32
	GOOS=windows GOARCH=amd64 go build $(GOFLAGS) -o ./bin/mop-$(VERSION)-windows-64.exe
	GOOS=windows GOARCH=386   go build $(GOFLAGS) -o ./bin/mop-$(VERSION)-windows-32.exe
